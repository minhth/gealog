package com.thm.gae_test.test;

import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class GeaTestApplication {
	
	public final static Logger logger = Logger.getLogger(GeaTestApplication.class.getName());

	public static void main(String[] args) {
		SpringApplication.run(GeaTestApplication.class, args);
	}
	
	@GetMapping("/")
	public String getDefault() {
		logger.info("Testing the log");
		return "default 2";
	}
}

